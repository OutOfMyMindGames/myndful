﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class BodyFocusVisualization : MonoBehaviour {
    private Material[] materials;
    public Transform sphere;
    private int spherePositionShaderPropertyID;
    private int sphereScaleShaderPropertyID;
    public Material holoMaterial;

    // Use this for initialization
    void Start () {
        spherePositionShaderPropertyID = Shader.PropertyToID("_spherePosition");
        sphereScaleShaderPropertyID = Shader.PropertyToID("_sphereScale");
    }
	
	// Update is called once per frame
	void Update () {
#if DEBUG
        if (sphere == null || holoMaterial == null)
        {
            print("---------- sphere or material is missing.");
            return;
        }
#endif
        holoMaterial.SetVector(spherePositionShaderPropertyID, sphere.position);
        holoMaterial.SetVector(sphereScaleShaderPropertyID, sphere.localScale);
    }

}
