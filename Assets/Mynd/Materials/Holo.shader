// Shader created with Shader Forge v1.20 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.20;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:3138,x:32719,y:32712,varname:node_3138,prsc:2|emission-3429-OUT;n:type:ShaderForge.SFN_Color,id:7241,x:32224,y:32872,ptovrint:False,ptlb:Color,ptin:_Color,varname:_Color,prsc:1,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.07843138,c2:0.3921569,c3:0.7843137,c4:1;n:type:ShaderForge.SFN_Fresnel,id:7746,x:31993,y:32726,varname:node_7746,prsc:2;n:type:ShaderForge.SFN_Multiply,id:1820,x:32463,y:32809,varname:node_1820,prsc:2|A-7746-OUT,B-7241-RGB;n:type:ShaderForge.SFN_Vector4Property,id:9223,x:31680,y:33266,ptovrint:False,ptlb:spherePosition,ptin:_spherePosition,varname:_spherePosition,prsc:2,glob:False,taghide:True,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0,v2:0,v3:0,v4:0;n:type:ShaderForge.SFN_FragmentPosition,id:6108,x:31680,y:33101,varname:node_6108,prsc:2;n:type:ShaderForge.SFN_Subtract,id:9039,x:31971,y:33138,varname:node_9039,prsc:2|A-6108-XYZ,B-9223-XYZ;n:type:ShaderForge.SFN_Length,id:5991,x:32136,y:33138,varname:distanceToSphere,prsc:1|IN-9039-OUT;n:type:ShaderForge.SFN_Clamp01,id:2922,x:32472,y:33138,varname:node_2922,prsc:2|IN-9055-OUT;n:type:ShaderForge.SFN_Vector4Property,id:2845,x:31680,y:33512,ptovrint:False,ptlb:sphereScale,ptin:_sphereScale,varname:_sphereScale,prsc:1,glob:False,taghide:True,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1,v2:1,v3:1,v4:1;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:9055,x:32358,y:33439,varname:node_9055,prsc:2|IN-5991-OUT,IMIN-1935-OUT,IMAX-6603-OUT,OMIN-5509-OUT,OMAX-1935-OUT;n:type:ShaderForge.SFN_Vector1,id:1935,x:32048,y:33405,varname:_zero,prsc:1,v1:0;n:type:ShaderForge.SFN_Vector1,id:5509,x:32110,y:33557,varname:_one,prsc:1,v1:1;n:type:ShaderForge.SFN_Multiply,id:6603,x:31903,y:33512,varname:node_6603,prsc:2|A-2845-X,B-240-OUT;n:type:ShaderForge.SFN_Vector1,id:240,x:31680,y:33722,varname:node_240,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Add,id:3429,x:32520,y:32998,varname:node_3429,prsc:2|A-1820-OUT,B-2922-OUT;proporder:7241-9223-2845;pass:END;sub:END;*/

Shader "Myndful/Holo" {
    Properties {
        _Color ("Color", Color) = (0.07843138,0.3921569,0.7843137,1)
        [HideInInspector]_spherePosition ("spherePosition", Vector) = (0,0,0,0)
        [HideInInspector]_sphereScale ("sphereScale", Vector) = (1,1,1,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers gles metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform half4 _Color;
            uniform float4 _spherePosition;
            uniform half4 _sphereScale;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
/////// Vectors:
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                half _zero = 0.0;
                half _one = 1.0;
                float3 emissive = (((1.0-max(0,dot(normalDirection, viewDirection)))*_Color.rgb)+saturate((_one + ( (length((i.posWorld.rgb-_spherePosition.rgb)) - _zero) * (_zero - _one) ) / ((_sphereScale.r*0.5) - _zero))));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
