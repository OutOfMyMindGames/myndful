﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Mynd
{
    public class SessionManager : MonoBehaviour
    {
        //
        private AudioSource audioSource;
        private Transform leftScanTarget;
        private Transform rightScanTarget;
        private float doubleClickInterval = 0.5f;
        //private SkinnedMeshRenderer avatarMesh;
        //private Animator avatarAnimator;

        // These values must be reset when experience is restarted
        private Mynd.State[] states;
        private Mynd.State currentState;
        private Mynd.State previousState;
        private Mynd.AudioState audioState;
        public int stateIndex;
        private int audioIndex;
        private float animTime;
        private float stateTime;
        private float audioTime;
        private float lastMouse0DownTime = 0f;

        // Use this for initialization
        void Start()
        {
            //avatarMesh = transform.FindChild("Male_Avatar/Male_LOD0").GetComponent<SkinnedMeshRenderer>();
            //avatarMesh.SetBlendShapeWeight(0, 100);
            //avatarAnimator = transform.FindChild("Male_Avatar").GetComponent<Animator>();
            audioSource = GetComponentInChildren<AudioSource>();
            if (leftScanTarget == null)
                leftScanTarget = transform.FindChild("Left Scan Target");
            LoadStates();
            transform.FindChild("Areas").gameObject.SetActive(false);
            Restart();
        }

        // Use this for initialization
        void Restart()
        {
            stateIndex = 0;
            audioIndex = 0;
            animTime = 0f;
            stateTime = 0f;
            audioTime = 0f;
            lastMouse0DownTime = 0f;
            previousState = states[stateIndex];
            currentState = states[stateIndex];
            audioState = currentState.audioStates[audioIndex];
            audioSource.PlayOneShot(audioState.clip);
        }


        void LoadStates()
        {
            List<Mynd.State> statesList = new List<Mynd.State>();
            AudioState[] audioStates;
            Transform scanTo;
            string name;

            //////
            name = "intro";
            audioStates = new AudioState[] {
                new Mynd.AudioState("silence", 10f),
                new Mynd.AudioState("f01_welcome", 2f),
                new Mynd.AudioState("f02_here_you_are_now", 2f),
                new Mynd.AudioState("f03_now_you_are_here", 2f),
                //new Mynd.AudioState("f04_choose_avatar"),
                new Mynd.AudioState("f05_tutorial_touchpad"),
                new Mynd.AudioState("f06_wonderful", 2f)
            };
            scanTo = transform.FindChild("Areas/begin");
            statesList.Add(new Mynd.State(name, audioStates, scanTo));

            //
            name = "hands on chest and belly";
            audioStates = new AudioState[] {
                new Mynd.AudioState("f07_hand_on_chest")
            };
            scanTo = transform.FindChild("Areas/hand_on_chest");
            statesList.Add(new Mynd.State(name, audioStates, scanTo));

            name = "hands on chest and belly";
            audioStates = new AudioState[] {
                new Mynd.AudioState("f08_hand_on_belly")
            };
            scanTo = transform.FindChild("Areas/hand_on_belly");
            statesList.Add(new Mynd.State(name, audioStates, scanTo));

            //
            name = "breath tutorial 1";
            audioStates = new AudioState[] {
                new Mynd.AudioState("f09_exhale"),
            };
            scanTo = transform.FindChild("Areas/exhale");
            statesList.Add(new Mynd.State(name, audioStates, scanTo));

            //
            name = "breath tutorial 2";
            audioStates = new AudioState[] {
                new Mynd.AudioState("f10_inhale"),
            };
            scanTo = transform.FindChild("Areas/inhale");
            statesList.Add(new Mynd.State(name, audioStates, scanTo));

            //
            name = "breath tutorial 3";
            audioStates = new AudioState[] {
                new Mynd.AudioState("f11_exhale"),
            };
            scanTo = transform.FindChild("Areas/exhale");
            statesList.Add(new Mynd.State(name, audioStates, scanTo));

            //
            name = "breath tutorial 4";
            audioStates = new AudioState[] {
                new Mynd.AudioState("f12_inhale"),
            };
            scanTo = transform.FindChild("Areas/inhale");
            statesList.Add(new Mynd.State(name, audioStates, scanTo));

            //
            name = "breath tutorial 5";
            audioStates = new AudioState[] {
                new Mynd.AudioState("f13_inhale_more", 2f),
                new Mynd.AudioState("f14_notice_belly"),
            };
            scanTo = transform.FindChild("Areas/inhale_more");
            statesList.Add(new Mynd.State(name, audioStates, scanTo));

            //
            name = "breath tutorial 6";
            audioStates = new AudioState[] {
                new Mynd.AudioState("f15_breath_out"),
            };
            scanTo = transform.FindChild("Areas/exhale");
            statesList.Add(new Mynd.State(name, audioStates, scanTo));

            //
            name = "begin scan";
            audioStates = new AudioState[] {
                new Mynd.AudioState("f16_begin_scan")
            };
            scanTo = transform.FindChild("Areas/begin_scan");
            statesList.Add(new Mynd.State(name, audioStates, scanTo));


            name = "notice sensations";
            audioStates = new AudioState[] {
                new Mynd.AudioState("f17_notice_sensations")
            };
            scanTo = transform.FindChild("Areas/notice_sensations");
            statesList.Add(new Mynd.State(name, audioStates, scanTo));

            //
            name = "no audio, move to above head";
            audioStates = new AudioState[] { };
            scanTo = transform.FindChild("Areas/headAbove");
            statesList.Add(new Mynd.State(name, audioStates, scanTo));

            //
            name = "scan_head";
            audioStates = new AudioState[] {
                new Mynd.AudioState("f18_scan_headTop"),
                new Mynd.AudioState("f19_notice_energy")
            };
            scanTo = transform.FindChild("Areas/headTop");
            statesList.Add(new Mynd.State(name, audioStates, scanTo));

            //
            name = "scan_neck";
            audioStates = new AudioState[] {
                new Mynd.AudioState("f20_bring_awareness_down"),
                new Mynd.AudioState("f21_scan_neck")

            };
            scanTo = transform.FindChild("Areas/neck");
            statesList.Add(new Mynd.State(name, audioStates, scanTo));

            //
            name = "scan_chest";
            audioStates = new AudioState[] {
                new Mynd.AudioState("f22_scan_chest"),

            };
            scanTo = transform.FindChild("Areas/chest");
            statesList.Add(new Mynd.State(name, audioStates, scanTo));

            //
            name = "scan_back";
            audioStates = new AudioState[] {
                new Mynd.AudioState("f23_scan_back"),
            };
            scanTo = transform.FindChild("Areas/back");
            statesList.Add(new Mynd.State(name, audioStates, scanTo));
            statesList[statesList.Count - 1].angle = 180f;
            
            //
            name = "scan_spine";
            audioStates = new AudioState[] {
                new Mynd.AudioState("f24_scan_spine"),
            };
            scanTo = transform.FindChild("Areas/spine");
            statesList.Add(new Mynd.State(name, audioStates, scanTo));
            statesList[statesList.Count - 1].angle = 180f;

            // All done. Convert the final list to an array for speed.
            this.states = statesList.ToArray();
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Restart();
            }
            
            
            // Check for double click
            if (Input.GetMouseButtonDown(0))
            {
                if(Time.time - lastMouse0DownTime < doubleClickInterval)
                {
                    print("Recentering.");
                    UnityEngine.VR.InputTracking.Recenter();
                }
                lastMouse0DownTime = Time.time;
            }


            // Animate
            animTime += Time.deltaTime;
            float transitionPercent = animTime * currentState.animLengthMultiplier;
            leftScanTarget.localPosition = Mynd.Lerp.Smoother(previousState.scanTo.localPosition, currentState.scanTo.localPosition, transitionPercent);
            leftScanTarget.localScale = Mynd.Lerp.Smoother(previousState.scanTo.localScale, currentState.scanTo.localScale, transitionPercent);
            float angle = Mynd.Lerp.Smoother(previousState.angle, currentState.angle, transitionPercent);
            transform.rotation = Quaternion.AngleAxis(angle, transform.up);

            audioTime += Time.deltaTime;
            if (audioTime > audioState.length && audioIndex < currentState.audioStates.Length - 1)
            {
                audioIndex++;
                audioTime = 0f;
                audioState = currentState.audioStates[audioIndex];
                audioSource.PlayOneShot(audioState.clip);
            }


            // Switch to next scan area
            stateTime += Time.deltaTime;
            if (stateTime > currentState.length && stateIndex < states.Length - 1)
            {
                stateIndex++;
                stateTime = 0f;
                animTime = 0f;
                previousState = currentState;
                currentState = states[stateIndex];
                audioTime = 0f;
                audioIndex = 0;
                audioState = currentState.audioStates[audioIndex];
                audioSource.PlayOneShot(audioState.clip);
            }
        }
    }
}

namespace Mynd
{
    public class State
    {
        public string name;
        public AudioState[] audioStates;
        public Transform scanTo;
        public string ease = "smoother";
        public float length;
        public float audioLength;
        public float animLength;
        public float animLengthMultiplier;
        public float angle = 0;
        
        public State(string name, AudioState[] audioStates, Transform scanTo, string ease)
        {
            this.name = name;
            this.audioStates = audioStates;
            this.scanTo = scanTo;
            this.ease = ease.ToLower();
            RecalculateLength();
        }

        public State(string name, AudioState[] audioStates, Transform scanTo)
        {
            this.name = name;
            this.audioStates = audioStates;
            this.scanTo = scanTo;
            RecalculateLength();
        }

        public void RecalculateLength()
        {
            this.audioLength = 0f;
            foreach (AudioState audioState in this.audioStates)
            {
                this.audioLength += audioState.length;
            }
            this.animLength = audioLength;
            this.animLengthMultiplier = 1f / this.animLength;
            this.length = audioLength;
        }
    }

    public class AudioState
    {
        public AudioClip clip;
        public string path;
        public float endDelay = 4f;
        public float length;
        
        public AudioState(string audioClipPath, float endDelay)
        {
            this.path = audioClipPath;
            this.clip = Resources.Load("Sounds/" + audioClipPath, typeof(AudioClip)) as AudioClip;
            this.endDelay = endDelay;
            this.length = this.clip.length + this.endDelay;
        }
        
        public AudioState(string audioClipPath)
        {
            this.path = audioClipPath;
            this.clip = Resources.Load("Sounds/" + audioClipPath, typeof(AudioClip)) as AudioClip;
            this.length = this.clip.length + this.endDelay;
        }
    }

    public static class Lerp
    {
        public static Vector3 Smoother(Vector3 a, Vector3 b, float t)
        {
            t = t * t * t * (t * (6f * t - 15f) + 10f);
            return Vector3.Lerp(a, b, t);
        }

        public static float Smoother(float a, float b, float t)
        {
            t = t * t * t * (t * (6f * t - 15f) + 10f);
            return Mathf.Lerp(a, b, t);
        }
    }
}
